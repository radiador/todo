//
//  EditTaskViewController.swift
//  TODOApp
//
//  Created by formador on 19/3/19.
//  Copyright © 2019 formador. All rights reserved.
//

import UIKit

protocol EditTaskViewControllerDelegate: class {
    
    func editFinished()
}

class EditTaskViewController: UIViewController {
    
    var task: Task?
    weak var delegate: EditTaskViewControllerDelegate?
    
    @IBOutlet weak var nameTxt: UITextField!
    @IBOutlet weak var typePickerView: UIPickerView!
    
    override func viewDidLoad() {
        super.viewDidLoad()

        typePickerView.dataSource = self
        typePickerView.delegate = self
    }

    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        nameTxt.text = task?.name
        
        var typeIndexSelected = 0
        
        for type in TaskType.allCases {
            
            if type == task?.type {
                break
            }
            
            typeIndexSelected += 1
        }
        
        var priorityIndexSelected = 0
        
        for priority in Priority.allCases {
            
            if priority == task?.priority {
                break
            }
            
            priorityIndexSelected += 1
        }
        
        typePickerView.selectRow(typeIndexSelected, inComponent: 0, animated: false)
        typePickerView.selectRow(priorityIndexSelected, inComponent: 1, animated: false)

    }
    
    @IBAction func okButtonAction(_ sender: UIButton) {
        
        if checkDescription() {
            
            task?.name = nameTxt.text ?? ""
            
            task?.type = TaskType.allCases[typePickerView.selectedRow(inComponent: 0)]
            task?.priority = Priority.allCases[typePickerView.selectedRow(inComponent: 1)]

            delegate?.editFinished()
        } else {
            
            let wrongDescriptionAlertView = UIAlertController(title: "Error", message: "Description not valid", preferredStyle: .alert)
            
            let okAlertAction = UIAlertAction(title: "OK", style: .default) { _ in
                }
            
            wrongDescriptionAlertView.addAction(okAlertAction)
            
            present(wrongDescriptionAlertView, animated: true, completion: nil)
        
        }
    }
    
    
    private func checkDescription() -> Bool {
        
        return (nameTxt.text ?? "").count > 2
    }
}

extension EditTaskViewController: UIPickerViewDataSource {
    
    func numberOfComponents(in pickerView: UIPickerView) -> Int {
        
        return 2
    }
    
    func pickerView(_ pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int {
        
        switch component {
        case 0:
            return TaskType.allCases.count
        case 1:
            return Priority.allCases.count
        default:
            return 0
        }
    }
}

extension EditTaskViewController: UIPickerViewDelegate {
    
    func pickerView(_ pickerView: UIPickerView, titleForRow row: Int, forComponent component: Int) -> String? {
        
        switch component {
        case 0:
            return TaskType.allCases[row].rawValue
        case 1:
            return Priority.allCases[row].rawValue
        default:
            return ""
        }
    }
}
