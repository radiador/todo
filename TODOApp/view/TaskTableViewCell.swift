//
//  TaskTableViewCell.swift
//  TODOApp
//
//  Created by formador on 19/3/19.
//  Copyright © 2019 formador. All rights reserved.
//

import UIKit

class TaskTableViewCell: UITableViewCell {

    @IBOutlet weak var nameLb: UILabel!
    @IBOutlet weak var priorityLb: UILabel!
    @IBOutlet weak var typeLb: UILabel!
        
    func configureCell(task: Task) {
        
        nameLb.text = task.name
        priorityLb.text = task.priority.rawValue
        typeLb.text = task.type.rawValue
    }
    
}
