//
//  TaskListViewController.swift
//  TODOApp
//
//  Created by formador on 19/3/19.
//  Copyright © 2019 formador. All rights reserved.
//

import UIKit

class TaskListViewController: UIViewController {
    
    @IBOutlet weak var taskTableView: UITableView!
    
    var taskList = [Task]()
    

    override func viewDidLoad() {
        super.viewDidLoad()

        taskTableView.register((UINib(nibName: "TaskTableViewCell", bundle: nil)), forCellReuseIdentifier: "taskCellIdentifier")
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        
        if segue.identifier == "editTaskSegueIdentifier" {
            
            let task = Task(name: "", type: .Undefined, priority: .Normal)
            
            taskList.append(task)
            
            let editTaskViewController = segue.destination as? EditTaskViewController
            
            editTaskViewController?.delegate = self
            
            editTaskViewController?.task = task
            
            taskTableView.reloadData()
        }
    }
    
    @IBAction func exitButtonAction(_ sender: Any) {
        
        dismiss(animated: true, completion: nil)
    }
    
}

extension TaskListViewController: UITableViewDataSource {
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        return taskList.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell = tableView.dequeueReusableCell(withIdentifier: "taskCellIdentifier", for: indexPath) as? TaskTableViewCell
        
        cell?.configureCell(task: taskList[indexPath.row])
        
        return cell ?? TaskTableViewCell()
    }
}

extension TaskListViewController: UITableViewDelegate {
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        let task = taskList[indexPath.row]
        
        let editTaskViewController = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "editTaskIdentifier") as? EditTaskViewController
        
        guard let editTaskViewControllerTmp = editTaskViewController else { return }
        
        editTaskViewControllerTmp.task = task
        
        editTaskViewControllerTmp.modalPresentationStyle = .formSheet
        
        editTaskViewControllerTmp.delegate = self
        
        present(editTaskViewControllerTmp, animated: true, completion: nil)
        
        let presentedVC = presentedViewController
        
        print("\(presentedViewController)")
    }
}

extension TaskListViewController: EditTaskViewControllerDelegate {
    
    func editFinished() {
        
        taskTableView.reloadData()
        
        dismiss(animated: true, completion: nil)
        
        print("Nda")
    }
}
