//
//  Task.swift
//  TODOApp
//
//  Created by formador on 19/3/19.
//  Copyright © 2019 formador. All rights reserved.
//

import Foundation

enum TaskType: String, CaseIterable {
    
    case House
    case Work
    case Family
    case Friends
    case Undefined
}

enum Priority: String, CaseIterable {
    
    case High
    case Normal
    case Low
}

class Task {
    
    var name: String
    var type: TaskType
    var priority: Priority
    
    init(name: String, type: TaskType, priority: Priority) {
        self.name = name
        self.type = type
        self.priority = priority
    }
}
